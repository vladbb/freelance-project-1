<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class PersonalController extends Controller
{
    public function index()
    {
        $users = User::all();
        $pageTitle = "Наши сотрудники";

        return view('about.personal', compact('users', 'pageTitle'));
    }
}
