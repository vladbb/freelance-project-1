<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PartnersController extends Controller
{
    public function index()
    {
        $pageTitle = "Наши партнеры";

        return view('about.partners', compact('pageTitle'));
    }
}
