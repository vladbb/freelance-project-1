<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AttestatsController extends Controller
{
    public function index()
    {
        $pageTitle = 'Аттестаты';
        
        return view('about.attestats', compact('pageTitle'));
    }
}
