<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Feedback;

class FeedBackController extends Controller
{
    public function index()
    {
        $feedbacks = Feedback::all();
        $feedbacks = $feedbacks->where('status', 'Новый');
        
        $pageTitle = "Отзывы о компании";

        return view('about.feedback', compact('feedbacks', 'pageTitle'));
    }
}
