<?php

namespace App\Http\Controllers\About;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\About\FeedbackRequest;
use App\Models\Feedback;

class SendFeedbackController extends Controller
{
    public function send(FeedbackRequest $request)
    {
        $data = $request->validated();
        
        Feedback::create([
            'fio' => $data['fio'],
            'message' => $data['message'],
            'rate' => $data['rate'],
            'status' => 'Новый',
        ]);

        return redirect()->route('about.feedback');
    }
}
