<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\User\ChangePasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class ChangePasswordController extends Controller
{
    public function edit(ChangePasswordRequest $request, User $user)
    {
        $data = $request->validated();

        $current_password = $user->password;     

        if(Hash::check($data['old_password'], $current_password))
        {           
            $user_id = $user->id;                       
            $obj_user = User::find($user_id);
            $obj_user->password = Hash::make($data['password']);
            $obj_user->save(); 

            Auth::logout();
        }

        return redirect()->route('main');
    }
}
