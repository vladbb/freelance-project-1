<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $pageTitle = 'Личный кабинет';

        return view('user.main', compact('user', 'pageTitle'));
    }
}
