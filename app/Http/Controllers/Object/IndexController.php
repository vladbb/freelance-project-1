<?php

namespace App\Http\Controllers\Object;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ImpObject;

class IndexController extends Controller
{
    public function index()
    {
        $pageTitle = "Реализованные объекты";
        $objects = ImpObject::all();

        return view('object.index', compact('pageTitle', 'objects'));
    }
}
