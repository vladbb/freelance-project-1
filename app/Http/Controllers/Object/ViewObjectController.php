<?php

namespace App\Http\Controllers\Object;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ImpObject;

class ViewObjectController extends Controller
{
    public function index(Request $request)
    {
        $objects = ImpObject::all();
        $id = (explode('/', $request->getRequestUri())[2]);
        $impObject = ImpObject::find($id);
        $pageTitle = "Реализованные объекты";

        return view('object.view', compact('pageTitle', 'objects', 'impObject'));
    }
}
