<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        $pageTitle = 'Главная';

        return view('welcome', compact('pageTitle'));
    }
}
