<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Category;

class IndexController extends Controller
{
    public function index()
    {
        $news = News::all();
        $news = $news->where('category_id', '1');
        
        $pageTitle = "Корпоративные новости";

        return view('news.index', compact('pageTitle', 'news'));
    }
}
