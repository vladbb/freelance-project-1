<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class TopicController extends Controller
{
    public function index()
    {
        $news = News::all();
        $news = $news->where('category_id', '2');
        
        $pageTitle = "Строительная рубрика";

        return view('news.topic', compact('pageTitle', 'news'));
    }
}
