<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class ViewTopicController extends Controller
{
    public function view(Request $request)
    {
        $id = (explode('/', $request->getRequestUri())[2]);
        $post = News::find($id);
        
        $pageTitle = "Строительная рубрика";
        
        return view('news.view_topic', compact('pageTitle', 'post'));
    }
}
