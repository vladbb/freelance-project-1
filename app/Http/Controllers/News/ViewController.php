<?php

namespace App\Http\Controllers\News;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;

class ViewController extends Controller
{
    public function view(Request $request)
    {
        $id = (explode('/', $request->getRequestUri())[2]);
        $post = News::find($id);
        
        $pageTitle = "Корпоративные новости";
        
        return view('news.view', compact('pageTitle', 'post'));
    }
}
