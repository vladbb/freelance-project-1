<?php

namespace App\Http\Controllers\Contact;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index()
    {
        $pageTitle = "Наши контакты";

        return view('contacts.index', compact('pageTitle'));
    }
}
