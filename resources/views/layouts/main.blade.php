<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ $pageTitle }}</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css' rel='stylesheet'>
    <link href='https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' rel='stylesheet'>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js"></script>
</head>
<body>
  <div class="wrapper">
    @section('header')
      <div class="header">
        <header class="p-2 text-white">
          <div class="container">
            <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
              <a href="/" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
                <img src="{{ asset('storage/images/main_logo.jpg') }}" alt="" class="main-logo">
              </a>

              <div class="row ms-4 me-4 nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                  <div class="col">
                      <div class="row">
                          <p class="mb-0 fw-bold fs-5"><i class='bx bxs-phone'></i> +375 (17) 567-89-10</p>
                      </div>
                      <div class="row">
                          <p class="mb-0 fw-bold fs-5"><i class='bx bx-envelope'></i> toresproect@gmail.com</p>
                      </div>
                  </div>
              </div>

              <div class="ow ms-4 me-4 nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
                <a href="" class="auth-link me-2 p-2 fw-bold fs-5" data-bs-toggle="modal" data-bs-target="#call">Заказать звонок <i class='bx bx-phone'></i></a>
              </div>

              <div class="text-en">
                @auth('web')
                  <div class="row">
                    <div class="col-8">
                      <a href="{{ route('user.index') }}" class="auth-link fw-bold fs-5">Профиль</ф>
                    </div>
                    <div class="col">
                      <form class="col-4" action="{{ route('logout') }}" method="post">
                        @csrf
                        <button type="submit" class="auth-link p-2 fw-bold fs-5 bg-danger">Выйти</butt>
                      </form>
                    </div>
                  </div>
                @endauth

                @guest('web')
                  <a class="auth-link me-2 p-2 fw-bold fs-5" data-bs-toggle="modal" data-bs-target="#register">Зарегистрироваться</ф>
                  <a class="auth-link p-2 fw-bold fs-5" data-bs-toggle="modal" data-bs-target="#login">Войти</a>
                @endguest
              </div>
            </div>
          </div>
        </header>

        <nav class="header-nav p-0">
          <div class="container">
            <ul class="menu d-flex align-items-center justify-content-center">
              <li><a href="{{ route('main') }}" class="pe-4 ps-4 p-2 fw-bold fs-5 {{ Request::path() == '/' ? 'yellow' : 'gray' }}">ГЛАВНАЯ</a></li>
              <li>
                <a href="{{ route('about.personal') }}" class="pe-4 ps-4 p-2 fw-bold fs-5 {{ str_contains(Request::path(), 'about') ? 'yellow' : 'gray' }}">О НАС</a>

                <ul class="">
                  <li><a href="{{ route('about.personal') }}" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Сотрудники</a></li>
                  <li><a href="{{ route('about.attestats') }}" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Аттестаты</a></li>
                  <li><a href="{{ route('about.partners') }}" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Партнеры</a></li>
                  <li><a href="{{ route('about.feedback') }}" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Отзывы</a></li>
                </ul>
                
              </li>
              <li>
                <a href="" class="pe-4 ps-4 p-2 fw-bold fs-5 {{ str_contains(Request::path(), 'services') ? 'yellow' : 'gray' }}">УСЛУГИ</a>

                <ul class="">
                  <li><a href="" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Разработка предпроектной документации</a></li>
                  <li><a href="" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Техническое обследование строительных конструкций и инженерных сетей</a></li>
                  <li><a href="" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Разработка проектной документации по капитальному ремонту жилых домов</a></li>
                  <li><a href="" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Авторский надзор</a></li>
                </ul>
                
              </li>
              <li><a href="{{ route('object.index') }}" class="pe-4 ps-4 p-2 fw-bold fs-5 {{ str_contains(Request::path(), 'objects') ? 'yellow' : 'gray' }}">РЕАЛИЗОВАННЫЕ ОБЪЕКТЫ</a></li>
              <li>
                <a href="{{ route('news.index') }}" class="pe-4 ps-4 p-2 fw-bold fs-5 {{ str_contains(Request::path(), 'news') ? 'yellow' : 'gray' }}">НОВОСТИ</a>

                <ul class="">
                  <li><a href="{{ route('news.index') }}" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Корпоративные новости</a></li>
                  <li><a href="{{ route('topic.index') }}" class="ps-4 pe-4 p-2 fw-bold fs-5 gray">Строительная рубрика</a></li>
                </ul>
                
              </li>
              <li><a href="{{ route('contacts.index') }}" class="pe-4 ps-4 p-2 fw-bold fs-5 {{ str_contains(Request::path(), 'contacts') ? 'yellow' : 'gray' }}">КОНТАКТЫ</a></li>
            </ul>
          </div>
        </nav>
      </div>
    @show

    
    <section class="main">
      <div class="container p-3 mt-4 mb-4">
        @yield('content')
      </div>
    </section>

    @section('footer')
      <div class="footer text-white">
        <div class="container">
          <footer class="py-2 my-3">
            <div class="row">
              <div class="col">
                <a href="/" class="d-flex align-items-center link-dark text-decoration-none">
                  <img src="{{ asset('storage/images/main_logo.jpg') }}" alt="" class="main-logo">
                </a>
              </div>

              <div class="col">
                <h5>О нас</h5>
                <ul class="nav flex-column">
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Сотрудники</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Аттестаты</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Партнеры</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Отзывы</a></li>
                </ul>
              </div>

              <div class="col">
                <h5>Услуги</h5>
                <ul class="nav flex-column">
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Разработка предпроектной документации</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Техническое обследование строительных конструкций и инженерных сетей</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Разработка проектной документации по капитальному ремонту жилых домов</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Авторский надзор</a></li>
                </ul>
              </div>

              <div class="col">
                <h5>Рализованные проекты</h5>
              </div>

              <div class="col">
                <h5>Новости</h5>
                <ul class="nav flex-column">
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Корпоративные новости</a></li>
                  <li class="nav-item mb-2"><a href="#" class="nav-link p-0 text-white">Строительная рубрика</a></li>
                </ul>
              </div>

              <div class="col">
                <h5>Контакты</h5>
              </div>
            </div>

            <div class="row">
              <div class="col text-end">
                <p>Мы в социальных сетях</p>
              </div>
            </div>
          </footer>
        </div>
      </div>
    @show

    <div class="modal" id="register">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal body -->
          <div class="modal-body col-10">
            <div>
              <h2 class="fw-bold mb-4 mt-4">Регистрация</h2>
            </div>
            
            <form action="{{ route('register') }}" method="post">
              @csrf
              <div class="mb-3">
                <input type="text" class="form-control" id="text" name="name" aria-describedby="nameHelp" placeholder="Введите ваше имя">
                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="mb-3">
                <input type="text" class="form-control" id="surname" name="surname" aria-describedby="surnameHelp" placeholder="Введите вашу фамилию">
                @error('surname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="mb-3">
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Введите ваш email адрес">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="mb-3">
                <input type="password" class="form-control" id="password" name="password" placeholder="Введите пароль">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="mb-3">
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Повторите введенный пароль">
                @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <button type="submit" class="form-button mb-4 fs-5 fw-bold">Зарегистрироваться</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="login">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal body -->
          <div class="modal-body col-10">
            <div>
              <h2 class="fw-bold mb-4 mt-4">Вход</h2>
            </div>
            
            <form action="{{ route('login') }}" method="post">
              @csrf
              <div class="mb-3">
                <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Введите ваш email адрес">
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="mb-3">
                <input type="password" class="form-control" id="password" name="password" placeholder="Введите пароль">
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <div class="mb-3 form-check">
                <input type="checkbox" class="form-check-input" id="check" name="check">
                <label class="form-check-label" for="check">Запомнить пароль</label>
              </div>
              <button type="submit" class="form-button mb-4 fs-5 fw-bold">Войти</button>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div class="modal" id="call">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal body -->
          <div class="modal-body col-10">
            <div>
              <h2 class="fw-bold mb-4 mt-4">Заказать звонок специалисту</h2>
            </div>
            
            <form action="" method="post">
              @csrf
              <div class="mb-3">
                <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp" placeholder="Введите ваше имя">
              </div>

              <div class="mb-3">
                <input type="password" class="form-control" id="password" name="password" placeholder="Введите ваш телефон">
              </div>

              <button type="submit" class="form-button mb-4 fs-5 fw-bold">Заказать</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
 

  @section('scripts')
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
  @show
</body>
</html>