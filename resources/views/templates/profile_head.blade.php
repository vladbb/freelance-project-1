<div class="row">
    <div class="col-4 mb-4">
        <img class="profile-logo rounded-circle" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
    </div>
    <div class="col-8">
        <div class="display-5 h1 fw-bold">Личный кабинет</div>
    </div>
</div>