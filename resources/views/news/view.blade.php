@extends('layouts.main')

@section('content')
    @include('templates.page_title')

    <div class="row text-center">
        <div class="col">
            <h2 class="mb-3 fw-bold">{{ $post->title}}</h2>
            <img src="{{ $post->image }}" alt="" class="mb-3">
            <h4 class="text-start">{{ $post->text }}</h4>
        </div>
    </div>
@endsection