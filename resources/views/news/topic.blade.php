@extends('layouts.main')

@section('content')
    @include('templates.page_title')

    @foreach($news as $post)
        <div class="row mb-4 text-center">
            <div class="col">
                <h2 class="mb-3 fw-bold">{{ $post->title }}</h3>
                <img src="{{ $post->image }}" alt="" class="mb-3">
                <h4 class="text-start">{{ $post->description }}</h4>
                <a href="{{ route('news.view', $post->id) }}" class="text-start">Читать полностью >></a>
            </div>
        </div>
    @endforeach
@endsection