@extends('layouts.main')

@section('content')
    <div class="profile">
        @include('templates.profile_head')

        <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">Профиль</button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        <form class="col-5 mt-4" action="{{ route('admin.edit', $user->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mb-3">
                                <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp" placeholder="Имя" value="{{ $user->name }}">
                            </div>

                            <div class="mb-3">
                                <input type="text" class="form-control" id="surname" name="surname" aria-describedby="surnameHelp" placeholder="Фамилия" value="{{ $user->surname }}">
                            </div>

                            <div class="mb-3">
                                <input type="text" class="form-control" id="lastname" name="lastname" aria-describedby="lastnameHelp" placeholder="Отчество" value="{{ $user->lastname }}">
                            </div>

                            <div class="mb-3">
                                <input type="text" class="form-control" id="held_post" name="held_post" aria-describedby="held_postHelp" placeholder="Должность" value="{{ $user->held_post }}">
                            </div>

                            <button type="submit" class="form-button mb-4 fs-5 fw-bold">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">Смена пароля</button>
                </h2>
                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                    <form class="col-5 mt-4" action="{{ route('user.password', $user->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mb-3">
                                <input type="password" class="form-control" id="password" name="old_password" placeholder="Старый пароль">
                            </div>

                            <div class="mb-3">
                                <input type="password" class="form-control" id="new_password" name="password" placeholder="Новый пароль">
                            </div>

                            <div class="mb-3">
                                <input type="password" class="form-control" id="new_password_confirmation" name="password_confirmation" placeholder="Повторите новый пароль">
                            </div>

                            <button type="submit" class="form-button mb-4 fs-5 fw-bold">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseOne">Пользователи</button>
                </h2>
                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseOne">Отзывы</button>
                </h2>
                <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseOne">Входящие</button>
                </h2>
                <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection