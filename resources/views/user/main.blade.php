@extends('layouts.main')

@section('content')
    <div class="profile">
        @include('templates.profile_head')
        <div class="accordion accordion-flush" id="accordionFlushExample">
            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">Профиль</button>
                </h2>
                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        <form class="col-5 mt-4" action="{{ route('user.edit', $user->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mb-3">
                                <input type="text" class="form-control" id="name" name="name" aria-describedby="nameHelp" placeholder="Имя" value="{{ $user->name }}">
                            </div>

                            <div class="mb-3">
                                <input type="text" class="form-control" id="surname" name="surname" aria-describedby="surnameHelp" placeholder="Фамилия" value="{{ $user->surname }}">
                            </div>

                            <div class="mb-3">
                                <input type="text" class="form-control" id="lastname" name="lastname" aria-describedby="lastnameHelp" placeholder="Отчество" value="{{ $user->lastname }}">
                            </div>

                            <div class="mb-3">
                                <input type="text" class="form-control" id="company" name="company" aria-describedby="companyHelp" placeholder="Организация" value="{{ $user->company }}">
                            </div>

                            <div class="mb-3">
                                <input type="tel" class="form-control" id="phone" name="phone" aria-describedby="phoneHelp" placeholder="Телефон" value="{{ $user->phone }}">
                            </div>
                            <button type="submit" class="form-button mb-4 fs-5 fw-bold">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">Смена пароля</button>
                </h2>
                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        <form class="col-5 mt-4" action="{{ route('user.password', $user->id) }}" method="post">
                            @csrf
                            @method('PATCH')
                            <div class="mb-3">
                                <input type="password" class="form-control" id="password" name="old_password" placeholder="Старый пароль">
                            </div>

                            <div class="mb-3">
                                <input type="password" class="form-control" id="new_password" name="password" placeholder="Новый пароль">
                            </div>

                            <div class="mb-3">
                                <input type="password" class="form-control" id="new_password_confirmation" name="password_confirmation" placeholder="Повторите новый пароль">
                            </div>

                            <button type="submit" class="form-button mb-4 fs-5 fw-bold">Сохранить</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseOne">Рассылка</button>
                </h2>
                <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        <div class="card p-3 fw-bold fs-5">
                            <form action="{{ route('user.mailing', $user->id) }}" method="post">
                                @csrf
                                @method('PATCH')
                                <div class="form-group">
                                    <label class="form-label">Вы хотите получать рассылку о новостях в компании?</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="mailing_news" id="mailing_news" value="yes" @if($user->mailing_news === "yes") checked @endif>
                                    <label class="form-check-label" for="mailing_news">Да</label>
                                </div>
                                <div class="form-check form-check-inline mb-4">
                                    <input class="form-check-input" type="radio" name="mailing_news" id="mailing_news" value="no" @if($user->mailing_news === "no") checked @endif>
                                    <label class="form-check-label" for="mailing_news">Нет</label>
                                </div>

                                <div class="form-group">
                                    <label class="form-label">Вы хотите получать рассылку из строительной рубрики?</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="mailing_topic" id="mailing_topic" value="yes" @if($user->mailing_topic === "yes") checked @endif>
                                    <label class="form-check-label" for="mailing_topic">Да</label>
                                </div>
                                <div class="form-check form-check-inline mb-4">
                                    <input class="form-check-input" type="radio" name="mailing_topic" id="mailing_topic" value="no" @if($user->mailing_topic === "no") checked @endif>
                                    <label class="form-check-label" for="mailing_topic">Нет</label>
                                </div>

                                <button type="submit" class="form-button mb-4 fs-5 fw-bold">Сохранить</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-headingOne">
                    <button class="text-white fw-bold fs-4 accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseOne">Техподдержка</button>
                </h2>
                <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body d-flex align-items-center justify-content-center">
                        <form class="col-5 mt-4" action="">
                            <div class="mb-3">
                                <input type="text" class="form-control" id="topic" name="topic" placeholder="Тема">
                            </div>
                            <div class="mb-3">
                                <textarea class="form-control" name="message" id="message" rows="5"></textarea>
                            </div>

                            <button type="submit" class="form-button mb-4 fs-5 fw-bold">Отправить</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection