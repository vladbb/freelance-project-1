@extends('layouts.main')

@section('content')
    @include('templates.page_title')

    <div class="row">
        <div class="col-8">
            <div class="row">
                <div class="col-6">
                    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0341d6b40d144724129a02a1680d71ac5c6cfa67523d9e0c57feb5c6fb3f8dc0&amp;width=320&amp;height=390&amp;lang=ru_RU&amp;scroll=true"></script>
                </div>
                <div class="col-6">
                    <h2 class="fw-bold textx-center mb-5">Связаться с нами</h2>
                    <div class="row">
                        <div class="row">
                            <h5 class="fw-bold"><i class='bx bxs-phone color-yellow'></i> Телефон:</h5>
                        </div>
                        <p class="mb-4">+375 (17) 567-89-10</p>

                        <div class="row">
                            <h5 class="fw-bold"><i class='bx bx-map color-yellow'></i> Адрес:</h5>
                        </div>
                        <p class="mb-4">Улица Чкалова 14-405,<br> Минск 220039</p>

                        <div class="row">
                            <h5 class="fw-bold"><i class='bx bx-envelope color-yellow'></i> Email:</h5>
                        </div>
                        <p class="mb-4">toresproect@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <h2 class="fw-bold textx-center mb-4">Напишите нам</h2>

            <form action="" method="post">
                @csrf
                <div class="mb-3">
                    <label for="name" class="form-label">Имя</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>

                <div class="mb-3">
                    <label for="email" class="form-label">Email</label>
                    <input type="email" class="form-control" id="email" name="email">
                </div>

                <div class="mb-3">
                    <label for="message" class="form-label">Чем мы можем вам помочь?</label>
                    <textarea class="form-control" id="message" name="message" rows="3"></textarea>
                </div>

                <button type="submit" class="form-button mb-4 fs-5 fw-bold">Отправить</button>
            </form>
        </div>
    </div>
@endsection