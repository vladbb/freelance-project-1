@extends('layouts.main')

@section('content')
    @include('templates.page_title')

    <div class="row">
        @foreach($objects as $object)
            <div class="obj_card col-4 mb-4">
                <a href="{{ route('object.view', $object->id) }}">
                    <img src="{{ $object->image }}" alt="">
                </a>
            </div>
        @endforeach
    </div>
@endsection