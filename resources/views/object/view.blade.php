@extends('layouts.main')

@section('content')
    <div class="row">
        <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
            <div class="carousel-inner text-center mb-4">
                @foreach($objects as $object)
                    <div class="carousel-item @if($object->id == $impObject->id) active @endif" data-bs-interval="10000">
                        <img src="{{ $object->image }}" class="carousel-img mb-4" alt="...">
                        <div class="">
                            <p>{{ $object->title }}</p>
                            <p>{{ $object->description }}</p>
                        </div>
                    </div>
                @endforeach
            </div>
            <!-- <div class="carousel-indicators mt-4">
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
            </div> -->
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Предыдущий</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark"  data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Следующий</span>
            </button>
        </div>
    </div>
@endsection