@extends('layouts.main')

@section('content')
    <div class="row">
        <div class="col-11 text-center">
            <div class="display-6 h1 fw-bold mb-5 ps-5">{{ $pageTitle }}</div>
        </div>
        <div class="col-1">
            <a href="" data-bs-toggle="modal" data-bs-target="#feedback">
                <img class="feedback-img ps-0 pe-0" src="{{ asset('storage/images/feedback.png') }}" alt="">
            </a>
        </div>
    </div>

    @foreach($feedbacks as $feedback)
    <div class="row feedback-card mb-4 p-3">
        <div class="col-2">
            <img class="profile-logo rounded-circle" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">

            <div class="star-rating mt-3">
                <div class="star-rating__wrap">
                    <input class="star-rating__input" id="star-5" type="radio" name="rate" value="5" {{ $feedback->rate == '5' ? 'checked' : '' }}>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-5" title="Отлично"></label>

                    <input class="star-rating__input" id="star-4" type="radio" name="rate" value="4" {{ $feedback->rate == '4' ? 'checked' : '' }}>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-4" title="Хорошо"></label>

                    <input class="star-rating__input" id="star-3" type="radio" name="rate" value="3" {{ $feedback->rate == '3' ? 'checked' : '' }}>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-3" title="Удовлетворительно"></label>

                    <input class="star-rating__input" id="star-2" type="radio" name="rate" value="2" {{ $feedback->rate == '2' ? 'checked' : '' }}>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-2" title="Плохо"></label>

                    <input class="star-rating__input" id="star-1" type="radio" name="rate" value="1" {{ $feedback->rate == '1' ? 'checked' : '' }}>
                    <label class="star-rating__ico fa fa-star-o fa-lg" for="star-1" title="Ужасно"></label>
                </div>
            </div>
        </div>
        <div class="col-10 mt-2">
            <p class="feedback-fio fw-bold fs-4">{{ $feedback->fio }}</p>
            <p>{{ $feedback->message }}</p>
        </div>
    </div>
    @endforeach

    <div class="modal" id="feedback">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
          <!-- Modal body -->
          <div class="modal-body col-10">
            <div>
              <h2 class="fw-bold mb-4 mt-4">Оставить отзыв</h2>
            </div>
            
            <form action="{{ route('about.feedback.send') }}" method="post">
                @csrf
                <div class="mb-3">
                    <input type="text" class="form-control" id="fio" name="fio" aria-describedby="fioHelp" placeholder="Введите ваше ФИО">
                </div>

                <div class="mb-3">
                    <textarea class="form-control" name="message" id="message" rows="5" placeholder="Введите текст"></textarea>
                </div>

                <div class="star-rating mb-3">
                    <div class="star-rating__wrap">
                        <input class="star-rating__input" id="star-5" type="radio" name="rate" value="5">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-5" title="Отлично"></label>

                        <input class="star-rating__input" id="star-4" type="radio" name="rate" value="4">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-4" title="Хорошо"></label>

                        <input class="star-rating__input" id="star-3" type="radio" name="rate" value="3">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-3" title="Удовлетворительно"></label>

                        <input class="star-rating__input" id="star-2" type="radio" name="rate" value="2">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-2" title="Плохо"></label>

                        <input class="star-rating__input" id="star-1" type="radio" name="rate" value="1">
                        <label class="star-rating__ico fa fa-star-o fa-lg" for="star-1" title="Ужасно"></label>
                    </div>
                </div>

                <button type="submit" class="form-button mb-4 fs-5 fw-bold">Отправить</button>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection