@extends('layouts.main')

@section('content')
    @include('templates.page_title')
    <div class="row">
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="{{ asset('storage/images/attestats/1.jpeg') }}">
                <img class="img-fluid" src="{{ asset('storage/images/attestats/1.jpeg') }}" alt="...">
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6 thumb">
            <a data-fancybox="gallery" href="{{ asset('storage/images/attestats/2.jpeg') }}">
                <img class="img-fluid" src="{{ asset('storage/images/attestats/2.jpeg') }}" alt="...">
            </a>
        </div>
    </div>
@endsection