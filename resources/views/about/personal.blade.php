@extends('layouts.main')

@section('content')
    @include('templates.page_title')
    <div class="row">
        @foreach($users as $user)
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        <div class="col-3">
            <div class="card p-3 mb-4 align-items-center">
                <img class="profile-logo rounded-circle mt-3 personal-img" src="{{ asset('storage/images/profile-logo.svg') }}" alt="">
                <p class="mt-4 mb-1 fw-bold fs-4 text-center">{{ $user->surname }} {{ $user->name }} {{ $user->lastname }}</p>
                <p class="fs-5 held-post">{{ $user->held_post }}</p>
            </div>
        </div>
        @endforeach
    </div>
@endsection