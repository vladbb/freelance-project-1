@extends('layouts.main')

@section('content')
    @include('templates.page_title')
    <div class="row">
        <div class="col-3">
            <img class="partner-img" src="{{ asset('storage/images/partners/1.jpg') }}" alt="">
        </div>
        <div class="col-3">
            <img class="partner-img" src="{{ asset('storage/images/partners/2.jpg') }}" alt="">
        </div>
        <div class="col-3">
            <img class="partner-img" src="{{ asset('storage/images/partners/3.png') }}" alt="">
        </div>
        <div class="col-3">
            <img class="partner-img" src="{{ asset('storage/images/partners/4.png') }}" alt="">
        </div>
    </div>
@endsection