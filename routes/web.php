<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\IndexController;
use App\Http\Controllers\Admin\IndexController as IndexAdmin;
use App\Http\Controllers\MainController;
use App\Http\Controllers\User\EditController;
use App\Http\Controllers\User\ChangePasswordController;
use App\Http\Controllers\User\MailingController;
use App\Http\Controllers\Admin\EditController as EditAdmin;
use App\Http\Controllers\About\PersonalController;
use App\Http\Controllers\About\AttestatsController;
use App\Http\Controllers\About\PartnersController;
use App\Http\Controllers\About\FeedBackController;
use App\Http\Controllers\About\SendFeedbackController;
use App\Http\Controllers\Object\IndexController as IndexObject;
use App\Http\Controllers\Object\ViewObjectController;
use App\Http\Controllers\News\IndexController as IndexNews;
use App\Http\Controllers\News\ViewController;
use App\Http\Controllers\News\TopicController;
use App\Http\Controllers\News\ViewTopicController;
use App\Http\Controllers\Contact\IndexController as IndexContacts;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index'])->name('main');

Route::group(['namespace' => 'About', 'prefix' => 'about'], function() {
    Route::get('/personal', [PersonalController::class, 'index'])->name('about.personal');
    Route::get('/attestats', [AttestatsController::class, 'index'])->name('about.attestats');
    Route::get('/partners', [PartnersController::class, 'index'])->name('about.partners');
    Route::get('/feedback', [FeedBackController::class, 'index'])->name('about.feedback');
    Route::post('/feedback/send', [SendFeedbackController::class, 'send'])->name('about.feedback.send');
});

Route::group(['namespace' => 'Object', 'prefix' => 'objects'], function() {
    Route::get('/', [IndexObject::class, 'index'])->name('object.index');
    Route::get('/{object}', [ViewObjectController::class, 'index'])->name('object.view');
});

Route::group(['namespace' => 'News', 'prefix' => 'news'], function() {
    Route::get('/', [IndexNews::class, 'index'])->name('news.index');
    Route::get('/{post}', [ViewController::class, 'view'])->name('news.view');
});

Route::group(['namespace' => 'News', 'prefix' => 'topic'], function() {
    Route::get('/', [TopicController::class, 'index'])->name('topic.index');
    Route::get('/{post}', [ViewTopicController::class, 'view'])->name('topic.view');
});

Route::group(['namespace' => 'Contacts', 'prefix' => 'contacts'], function() {
    Route::get('/', [IndexContacts::class, 'index'])->name('contacts.index');
});

Route::group(['middleware' => 'auth'], function() {
    Route::group(['namespace' => 'User', 'prefix' => 'user'], function() {
        Route::get('/', [IndexController::class, 'index'])->name('user.index');
        Route::patch('/{user}', [EditController::class, 'edit'])->name('user.edit');
        Route::patch('/{user}/password', [ChangePasswordController::class, 'edit'])->name('user.password');
        Route::patch('/{user}/mailing', [MailingController::class, 'edit'])->name('user.mailing');
    });
});



Route::group(['namespace' => 'Admin', 'prefix' => 'admin'], function() {
    Route::get('/', [IndexAdmin::class, 'index'])->name('admin.index');
    Route::patch('/{user}', [EditAdmin::class, 'edit'])->name('admin.edit');
});

Auth::routes();
